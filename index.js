/*
const http = require('http');

const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  res.end('Olá Mundo\n');
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
*/
const restify = require('restify');
const errs = require('restify-errors')

const server = restify.createServer({
  name: 'myapp',
  version: '1.0.0'
});

/** Conexão com o banco */
const knex = require('knex')({
  client: 'mysql',
  connection: {
    host : '127.0.0.1',
    user : 'root',
    password : 'usbw',
    database : 'crud_node'
  }
});

server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.queryParser());
server.use(restify.plugins.bodyParser());

server.get('/echo/:name', function (req, res, next) {
  res.send(req.params);
  return next();
});

server.listen(8000, function () {
  console.log('%s listening at %s', server.name, server.url);
});

/** Rotas para o Postman */
server.get('/read', function (req, res, next) {
  knex('usuario').then((dados)=>{
    res.send(dados);
  }), next;
});

server.post('/create', function (req, res, next) {
  knex('usuario') //Nome da tabela no banco
    .insert(req.body)  
    .then((dados)=>{
      res.send(dados);
    }), next;
});

server.get('/show/:id', function (req, res, next) {
  const {id} = req.params;
  knex('usuario')
  .where('id', id)
  .first()
  .then((dados)=>{
    if(!dados) return res.send(new errs.BadRequestError('Nada foi Encontrado!'));
    res.send(dados);
  }), next;
});

server.put('/update/:id', function (req, res, next) {
  const {id} = req.params;
  knex('usuario')
  .where('id', id)
  .update(req.body)
  .then((dados)=>{
    if(!dados) return res.send(new errs.BadRequestError('Nada foi Encontrado!'));
    res.send('Atualização Feita Com Sucesso');
  }), next;
});

server.del('/delete/:id', function (req, res, next) {
  const {id} = req.params;
  knex('usuario')
  .where('id', id)
  .delete()
  .then((dados)=>{
    if(!dados) return res.send(new errs.BadRequestError('Nada foi Encontrado!'));
    res.send('Atualização Feita Com Sucesso');
  }), next;
});

/** Rotas para views */
server.get('/', restify.plugins.serveStatic ({
  directory: './views',
  file: 'index.html'
}));